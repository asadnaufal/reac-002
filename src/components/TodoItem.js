import React from 'react';

import {View, Text, StyleSheet} from 'react-native';

const getStatusStyle = (status) => {
  switch (status) {
    case 'completed':
      return styles.statusCompleted;
    case 'cancelled':
      return styles.statusCancelled;
    default:
      return styles.statusPending;
  }
};

const TodoItem = (props) => {
  const {title, status} = props;

  return (
    <View style={styles.container}>
      <View style={styles.leftContent}>
        <Text numberOfLines={2} style={styles.title} ellipsizeMode="tail">
          {title}
        </Text>
        <View style={{height: 16}} />
        <Text
          style={StyleSheet.flatten([getStatusStyle(status), styles.status])}>
          {status}
        </Text>
      </View>
      <View style={styles.rightContent}>
        <Text style={styles.removeButtonTitle}>Remove</Text>
        <View style={{height: 16}} />
        <Text>{status === 'completed' ? '️️✔️️' : '️'}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    padding: 12,
    backgroundColor: '#3c3836',
    flexDirection: 'row',
    alignItems: 'center',
  },
  leftContent: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    maxWidth: '80%',
  },
  rightContent: {
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  title: {color: '#928374', fontSize: 24},
  description: {color: '#b57164'},
  status: {fontSize: 16, fontWeight: 'bold'},
  statusCompleted: {color: '#427b58'},
  statusPending: {color: '#076678'},
  statusCancelled: {color: '#9d0006'},
  removeButton: {
    backgroundColor: '#cc241d',
    padding: 8,
  },
  removeButtonTitle: {
    color: '#fbf1c7',
  },
  toggleCompleted: {
    backgroundColor: '#fbf1c7',
    height: 24,
    width: 24,
  },
});

export default TodoItem;
