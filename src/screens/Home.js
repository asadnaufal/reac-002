import React from 'react';
import {SafeAreaView, StyleSheet, ScrollView, View} from 'react-native';

import TodoItem from '../components/TodoItem';
import Header from '../components/Header';

const todos = [
  {
    title: 'belajar react native components',
    description: 'no description',
    status: 'pending',
  },
  {
    title: 'belajar react hooks',
    description: '-',
    status: 'pending',
  },
];

export const Home = (props) => {
  return (
    <SafeAreaView style={styles.container}>
      <Header
        title={`Today's todos ${
          todos.every((todo) => todo.status === 'completed') ? '🥳' : '😃'
        }`}
      />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.scrollView}>
        {todos.map((todo) => {
          return (
            <View key={todo.title} style={{marginBottom: 16}}>
              <TodoItem
                title={todo.title}
                description={todo.description}
                status={todo.status}
              />
            </View>
          );
        })}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#282828',
    flex: 1,
    padding: 8,
  },
  scrollView: {},
});
